export interface Register {
  name: string;
  phoneNumber: string;
  userPin: string;
  userEmail: string;
}
