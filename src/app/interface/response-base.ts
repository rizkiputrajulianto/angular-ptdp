export interface ResponseBaseLogin {
  jwtToken: string;
  message: string;
}

export interface ResponseBaseRegister<T> {
  message: string;
  data: T;
}
