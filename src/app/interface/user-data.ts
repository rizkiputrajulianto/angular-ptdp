export interface UserData {
  email: string;
  id: number;
  name: string;
  noHandphone: string;
  userPin: string;
}
