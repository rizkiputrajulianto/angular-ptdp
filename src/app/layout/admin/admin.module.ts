import { NgModule } from '@angular/core';
import { CommonModule, DecimalPipe } from '@angular/common';
import { AdminComponent } from './admin.component';
import { PartialsModule } from 'src/app/partials/partials.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminRoutingModule } from './admin-routing.module';
import { PlaygroundComponent } from './playground/playground.component';
import { CalculatorComponent } from 'src/app/component/calculator/calculator.component';
import { TopupComponent } from './topup/topup.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AdminComponent,
    PlaygroundComponent,
    CalculatorComponent,
    TopupComponent,
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    PartialsModule,
    FormsModule,
    NgbModule,
    ReactiveFormsModule,
  ],
  exports: [TopupComponent, CommonModule],
  bootstrap: [TopupComponent],
})
export class AdminModule {}
