import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CalculatorComponent } from 'src/app/component/calculator/calculator.component';
import { AuthGuardGuard } from 'src/app/guard/auth-guard.guard';
import { AdminComponent } from './admin.component';
import { PlaygroundComponent } from './playground/playground.component';
import { TopupComponent } from './topup/topup.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: 'playground',
        component: PlaygroundComponent,
      },
      {
        path: 'calculator',
        component: CalculatorComponent,
      },
      {
        path: 'topup',
        component: TopupComponent,
      },
    ],
    canLoad: [AuthGuardGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule {}
