import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
  PipeTransform,
} from '@angular/core';
import { DataTransaksiService } from 'src/app/services/get/data-transaksi.service';
import { DecimalPipe } from '@angular/common';
import { FormControl } from '@angular/forms';

import {
  firstValueFrom,
  interval,
  Observable,
  Subject,
  Subscription,
} from 'rxjs';
// import { map, startWith, takeUntil } from 'rxjs/operators';

export interface TransactionResponse {
  data: Data[];
  message: string;
}

export interface Data {
  id: number;
  noKartuETol: string;
  noReferensi: any;
  nominal: number;
  statusTransaksi: any;
  tanggalPembayaran: string;
}

interface Country {
  name: string;
  flag: string;
  area: number;
  population: number;
}

const COUNTRIES: Country[] = [
  {
    name: 'Russia',
    flag: 'f/f3/Flag_of_Russia.svg',
    area: 17075200,
    population: 146989754,
  },
  {
    name: 'Canada',
    flag: 'c/cf/Flag_of_Canada.svg',
    area: 9976140,
    population: 36624199,
  },
  {
    name: 'United States',
    flag: 'a/a4/Flag_of_the_United_States.svg',
    area: 9629091,
    population: 324459463,
  },
  {
    name: 'China',
    flag: 'f/fa/Flag_of_the_People%27s_Republic_of_China.svg',
    area: 9596960,
    population: 1409517397,
  },
];

// function search(text: string, pipe: PipeTransform): Country[] {
//   return COUNTRIES.filter((country) => {
//     const term = text.toLowerCase();
//     return (
//       country.name.toLowerCase().includes(term) ||
//       pipe.transform(country.area).includes(term) ||
//       pipe.transform(country.population).includes(term)
//     );
//   });
// }

function searchTransaction(text: string, data: Data[], pipe?: PipeTransform) {
  return data.filter((country) => {
    const term = text.toLowerCase();
    return (
      country.statusTransaksi?.toLowerCase().includes(term) ||
      country.noKartuETol?.toLowerCase().includes(term) ||
      country.noReferensi?.toLowerCase().includes(term)
    );
  });
}

@Component({
  selector: 'app-topup',
  templateUrl: './topup.component.html',
  styleUrls: ['./topup.component.scss'],
  providers: [DecimalPipe],
})
export class TopupComponent implements OnInit {
  TRANSACTION!: TransactionResponse;

  countries$!: Observable<Data[]>;
  filter = new FormControl('', { nonNullable: true });

  constructor(
    private dataService: DataTransaksiService,
    private pipe: DecimalPipe
  ) {}

  async ngOnInit() {
    try {
      const result = await firstValueFrom(
        this.dataService.getTransactionInformation()
      );
      // console.log(result);

      // this.countries$ = this.filter.valueChanges.pipe(
      //   startWith(''),
      //   map((text) => searchTransaction(text))
      // );

      // this.countries$ = this.filter.valueChanges.pipe(
      //   startWith(''),
      //   map((text: string) => searchTransaction(text, result, this.pipe))
      // );
    } catch (e) {
      console.log(e);
    }

    // this.isDestroy = this.timer.subscribe((val) => {
    //   console.log(val);
    // });
    // this.timer.pipe(takeUntil(this.isDestroySubject)).subscribe((val) => {
    //   console.log(val);
    // });
  }

  // ngOnDestroy(): void {
  //   // this.isDestroySubject.next(true);
  //   // this.isDestroySubject.complete();
  //   // this.isDestroy.unsubscribe();
  // }

  doOrder() {
    console.log('hehe');
  }

  timer = interval(100);
  isDestroy = new Subscription();
  isDestroySubject = new Subject();
}
