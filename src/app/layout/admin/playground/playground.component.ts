import { Component, OnInit } from '@angular/core';
import { Person } from 'src/app/person';
import { DataAccessService } from 'src/app/services/data-access.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-playground',
  templateUrl: './playground.component.html',
  styleUrls: ['./playground.component.scss'],
})
export class PlaygroundComponent implements OnInit {
  public name: string = '';
  public age: number = 0;
  public show: boolean = true;
  public update: boolean = false;

  public person: Person[] = [];
  public temp: Person = {
    name: '',
    age: 0,
  };

  constructor(private dataAccessService: DataAccessService) {}

  ngOnInit(): void {
    this.person = this.dataAccessService.getPerson();
  }

  sendData() {
    if (this.name == '') {
      Swal.fire({
        title: 'Error',
        icon: 'error',
        text: 'Nama Tidak Boleh Kosong',
        confirmButtonText: 'OK',
      });
    } else if (this.age < 0) {
      Swal.fire({
        title: 'Error',
        icon: 'error',
        text: 'Umur Tidak Boleh Bernilai Minus',
        confirmButtonText: 'OK',
      });
    } else {
      const payload = {
        name: this.name,
        age: this.age,
      };
      this.dataAccessService.addPerson(payload);
      this.name = '';
      this.age = 0;
    }
  }

  deleteData($event: Person) {
    this.person = this.dataAccessService.deletePerson($event);
  }

  updateData($event: Person) {
    this.name = $event.name;
    this.age = $event.age;
    this.temp = $event;
    this.update = true;
  }

  submitUpdate() {
    if (this.name == '') {
      Swal.fire({
        title: 'Error',
        icon: 'error',
        text: 'Nama Tidak Boleh Kosong',
        confirmButtonText: 'OK',
      });
    } else if (this.age < 0) {
      Swal.fire({
        title: 'Error',
        icon: 'error',
        text: 'Umur Tidak Boleh Bernilai Minus',
        confirmButtonText: 'OK',
      });
    } else {
      const payload = {
        name: this.name,
        age: this.age,
      };
      this.person = this.dataAccessService.updatePerson(payload, this.temp);
      this.update = false;
      this.name = '';
      this.age = 0;
    }
  }

  showForm() {
    this.show = !this.show;
  }
}
