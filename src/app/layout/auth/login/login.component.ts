import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Login } from 'src/app/interface/login';
import { AuthenticationService } from 'src/app/services/auth/authentication.service';
import { HotToastService } from '@ngneat/hot-toast';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { AnimationOptions } from 'ngx-lottie';
import { TranslatesService } from 'src/app/services/language/translates.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  lottieConfig: AnimationOptions = {
    // path: 'assets/lottie/75460-credit-card.json',
    path: 'assets/lottie/75145-astronauta.json',
  };

  loginData: Login = {
    phoneNumber: '',
    userPin: '',
  };

  isPass: boolean = true;

  formLogin!: FormGroup;

  constructor(
    private router: Router,
    private auth: AuthenticationService,
    private toast: HotToastService,
    private translator: TranslatesService
  ) {
    this.formLogin = new FormGroup({
      phoneNumber: new FormControl<string | null>('', {
        validators: [Validators.required, Validators.minLength(12)],
      }),
      userPin: new FormControl<string | null>('', {
        validators: [Validators.required, Validators.minLength(6)],
      }),
    });
  }

  changeLanguage(lang: string) {
    this.translator.changeLang(lang);
  }

  get errorController() {
    return this.formLogin.controls;
  }

  showType() {
    this.isPass = !this.isPass;
  }

  loginViaAngular() {
    const payload = {
      phoneNumber: this.formLogin.value.phoneNumber,
      userPin: this.formLogin.value.userPin,
    };
    this.auth.login(payload).subscribe({
      next: (val) => {
        console.log(val);
        this.toast.success(val.message);
        localStorage.setItem('TOKEN', val.jwtToken);
        this.auth.token = val.jwtToken;
        this.router.navigate(['/admin/playground']);
      },
      error: (error: any) => {
        console.log(error);
        this.toast.error(error.error);
      },
    });
  }

  // async loginUser() {
  //   try {
  //     await axios
  //       .post('http://localhost:6969/login', this.loginData)
  //       .then((response) => {
  //         console.log(response.data);
  //         Swal.fire({
  //           title: 'Berhasil Login',
  //           text: 'Selamat Datang ' + response.data.name,
  //           icon: 'success',
  //           confirmButtonText: 'OK',
  //         }).then((result) => {
  //           if (result.isConfirmed) {
  //             this.router.navigate(['/playground']);
  //           }
  //         });
  //       });
  //   } catch (error) {
  //     console.log(error);
  //   }
  // }
}
