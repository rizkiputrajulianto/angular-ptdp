import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Register } from 'src/app/interface/register';
import axios from 'axios';
import Swal from 'sweetalert2';
import { AuthenticationService } from 'src/app/services/auth/authentication.service';
import { HotToastService } from '@ngneat/hot-toast';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent {
  requestMember: Register = {
    name: '',
    phoneNumber: '',
    userEmail: '',
    userPin: '',
  };

  constructor(
    private router: Router,
    private auth: AuthenticationService,
    private toast: HotToastService
  ) {}

  sendDataViaAngular() {
    const payload = {
      name: this.requestMember.name,
      phoneNumber: this.requestMember.phoneNumber,
      userEmail: this.requestMember.userEmail,
      userPin: this.requestMember.userPin,
    };
    this.auth.register(payload).subscribe({
      next: (val) => {
        console.log(val);
        this.toast.success(`${val.data.name} has been Created`);
        this.router.navigate(['/auth/login']);
      },
      error: (err: any) => {
        console.log(err);
      },
    });
  }

  sendData() {
    axios
      .post('http://localhost:6969/register', this.requestMember)
      .then((response) => {
        Swal.fire({
          title: 'Berhasil Register Akun',
          text: 'Silahkan Login',
          icon: 'success',
          confirmButtonText: 'OK',
        }).then((result) => {
          if (result.isConfirmed) {
            this.router.navigate(['/login']);
          }
        });
      })
      .catch((error) => console.log(error));
  }

  // sendData() {
  //   this.regist
  //     .registerMember(this.requestMember)
  //     .subscribe((data) => console.log(data));
  // }
}
