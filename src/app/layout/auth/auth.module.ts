import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthComponent } from './auth.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HotToastModule } from '@ngneat/hot-toast';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AuthRoutingModule } from './auth-routing.module';
import { PartialsModule } from 'src/app/partials/partials.module';
import { LottieModule } from 'ngx-lottie';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [AuthComponent, LoginComponent, RegisterComponent],
  imports: [
    CommonModule,
    AuthRoutingModule,
    HotToastModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    PartialsModule,
    LottieModule,
    TranslateModule,
  ],
})
export class AuthModule {}
