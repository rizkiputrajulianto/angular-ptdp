import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HotToastService } from '@ngneat/hot-toast';
import { catchError, Observable, throwError } from 'rxjs';
import { AuthenticationService } from './auth/authentication.service';

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {
  constructor(
    private authService: AuthenticationService,
    private route: Router,
    private toast: HotToastService
  ) {
    console.log(this.authService.token);
  }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    console.log('hesoyam', req);
    console.log(this.authService.token);

    if (this.authService.token) {
      req = req.clone({
        headers: req.headers.set(
          'authorization',
          'Bearer ' + this.authService.token
        ),
      });
    }
    console.log(req);
    return next.handle(req).pipe(
      catchError((response: HttpErrorResponse) => {
        console.log(response);
        if (response.status == 401) {
          this.authService.token = '';
          localStorage.removeItem('TOKEN');
          this.toast.error('YAHHHH GAPUNYA AKSES YAAA');
          this.route.navigate(['/auth/login']);
        }

        return throwError(response);
      })
    );
  }
}
