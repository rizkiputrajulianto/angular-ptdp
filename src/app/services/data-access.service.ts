import { Injectable } from '@angular/core';
import { Person } from '../person';

@Injectable({
  providedIn: 'root',
})
export class DataAccessService {
  private person: Person[] = [
    {
      name: 'Rizki',
      age: 20,
    },
    {
      name: 'Putra',
      age: 24,
    },
  ];

  constructor() {}

  getPerson() {
    return this.person;
  }

  addPerson(payload: Person) {
    this.person.push(payload);
  }

  deletePerson(payload: Person) {
    this.person = this.person.filter((data) => data != payload);
    return this.person;
  }

  updatePerson(newData: Person, oldData: Person): Person[] {
    this.person = this.person.map((data) => {
      let newDataTemp = data;
      if (data === oldData) {
        newDataTemp = newData;
      }
      return newDataTemp;
    });
    return this.person;
  }
}
