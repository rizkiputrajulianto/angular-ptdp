import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class DataTransaksiService {
  private readonly baseUrl = environment.api;

  constructor(private http: HttpClient) {}

  getTransactionInformation() {
    // const token = localStorage.getItem('TOKEN');
    // const headers = new HttpHeaders({
    //   Authorization: 'Bearer ' + token,
    // });
    return this.http.get(`${this.baseUrl}/payment/all`).pipe(
      map((val: any) => {
        return val.data;
      })
    );
  }
}
