import { TestBed } from '@angular/core/testing';

import { DataTransaksiService } from './data-transaksi.service';

describe('DataTransaksiService', () => {
  let service: DataTransaksiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DataTransaksiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
