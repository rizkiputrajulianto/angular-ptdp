import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, tap } from 'rxjs';
import { Login } from 'src/app/interface/login';
import { Register } from 'src/app/interface/register';
import {
  ResponseBaseLogin,
  ResponseBaseRegister,
} from 'src/app/interface/response-base';
import { UserData } from 'src/app/interface/user-data';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  private _token: string = '';
  public isAuthenticate: BehaviorSubject<boolean> =
    new BehaviorSubject<boolean>(false);
  private readonly baseUrl = environment.api;

  constructor(private http: HttpClient) {
    this.loadToken();
  }

  /**
   *
   * @param payload
   * @returns
   */
  login(payload: Login): Observable<ResponseBaseLogin> {
    return this.http
      .post<ResponseBaseLogin>(`${this.baseUrl}/login`, payload)
      .pipe(
        tap(() => {
          this.isAuthenticate.next(true);
        })
      );
  }

  register(payload: Register): Observable<ResponseBaseRegister<UserData>> {
    return this.http.post<ResponseBaseRegister<UserData>>(
      `${this.baseUrl}/register`,
      payload
    );
  }

  loadToken() {
    const token = localStorage.getItem('TOKEN');
    if (token) {
      this.token = token;
      this.isAuthenticate.next(true);
    } else {
      this.isAuthenticate.next(false);
    }
  }

  clearToken() {
    localStorage.clear();
    this.isAuthenticate.next(false);
  }

  set token(ev: string) {
    this._token = ev;
  }

  get token() {
    return this._token;
  }
}
