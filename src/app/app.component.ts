import { Component, OnInit } from '@angular/core';
import { TranslatesService } from './services/language/translates.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'meet-pertama-angular';

  constructor(private translator: TranslatesService) {}
  ngOnInit(): void {
    this.translator.changeLang('yd');
  }
}
