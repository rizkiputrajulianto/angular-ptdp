import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanLoad,
  Route,
  Router,
  RouterStateSnapshot,
  UrlSegment,
  UrlTree,
} from '@angular/router';
import { filter, map, Observable } from 'rxjs';
import { AuthenticationService } from '../services/auth/authentication.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuardGuard implements CanLoad {
  constructor(private router: Router, private auth: AuthenticationService) {}

  canLoad() {
    return this.auth.isAuthenticate.pipe(
      filter((val) => val !== null),
      map((val) => {
        if (val) {
          return true;
        } else {
          this.router.navigate(['/auth/login']);
          return false;
        }
      })
    );
  }
}
