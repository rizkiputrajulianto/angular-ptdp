import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-card-name',
  templateUrl: './card-name.component.html',
  styleUrls: ['./card-name.component.scss'],
})
export class CardNameComponent {
  @Input() data = {
    name: 'a',
    age: 0,
  };

  @Output() cardValue = new EventEmitter();
  @Output() updateValue = new EventEmitter();

  constructor() {}

  doDelete() {
    this.cardValue.emit(this.data);
  }

  doUpdate() {
    this.updateValue.emit(this.data);
  }
}
