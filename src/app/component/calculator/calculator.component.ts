import { Component, OnInit } from '@angular/core';
import { Data } from '../data';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss'],
})
export class CalculatorComponent {
  public data: Data = {
    firstNumber: 0,
    secondNumber: 0,
  };

  constructor() {}

  tambah() {
    return this.data.firstNumber + this.data.secondNumber;
  }
}
