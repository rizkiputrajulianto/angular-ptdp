import { Component, OnInit } from '@angular/core';
import { DataSidebar } from 'src/app/interface/data-sidebar';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent {
  sidebarData: DataSidebar[] = [
    {
      namaPage: 'Top Up',
      link: '/admin/topup',
    },
    {
      namaPage: 'Playground',
      link: '/admin/playground',
    },
    {
      namaPage: 'Calculator',
      link: '/admin/calculator',
    },
    {
      namaPage: 'Logout',
      link: '/admin/auth/login',
    },
  ];

  constructor() {}
}
